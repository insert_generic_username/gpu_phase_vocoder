
A brief list of the tasks accomplished during that week.
Any issues that were encountered.
A brief list of what will be focused on during the next week.



Tasks accomplished:
    - Set up FFTW libraries, test code runs.
    - Looked through past papers to see how to handle Fourier transformations.

Issues encountered:
    - (Multi-channel FFT)


Focus for next week:
    - (Pitch shifter only)