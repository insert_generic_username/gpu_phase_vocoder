(Note that, for the future, )



>>>>>>>>>>>>>>>> Option 1a -- No Deinterlacing, Per-GPU-thread FFT, no buffering:

Assumptions:
    - STFT window size is power of 2



(#define macro to access sample within channel)


>>>>>>> GPU

fft_kernel:
    (Cooley-Tukey algorithm, allow recursion since Cuda CC >= 2.0)

polar_kernel:
    range over (increments of block size * grid size):

        pv_stage2 magnitude at i <- (abs of complex value from pv_stage1)
        pv_stage2 phase at i <- (arg of comple value from pv_stage 1)

...



>>>>>>> CPU

semitone_frequency_shift (n):
    (12th root of 2)^n


main:
    Load in WAV PCM 16-bit stream

    (pv_stage0) Allocate memory on GPU for input data, copy CPU->GPU

    (pv_stage1) Allocate memory for STFT frequency-domain results (complex numbers)
        Size: (STFT window size) * (# windows = (Length of audio - STFT_window_size) / (frame_offset))

    (pv_stage2) Allocate memory for STFT amplitude and phase information (in total, same size
        as previous array)

    GPU: Launch fft_kernel on all possible overlapping windows
        (1 thread/block unless audio data too large)

    GPU: Launch polar_kernel

    GPU: Over each frequency magnitude window, find peak
        Hardcode formula from <site> to find approximation to n, then round
            to nearest n to obtain frequency

    GPU: (Somehow shift the frequency):
        (This may just involve stretching the window and resampling)

    (pv_stage3) Allocate memory for inverse STFT

    (convert back to rectangular form depending on how pitch shifting went)

    GPU: launch ifft_kernel (similar to fft_kernel but exponent goes in reverse direction)

    GPU: Interpolate in time-domain (??)

    Transfer data GPU -> CPU

    free all pv_stages

    Write WAV PCM 16-bit stream

