
import sys
import numpy as np
import scipy.io.wavfile as wav
import scipy.signal as ss
from utility import pcm2float




def main():
    # Read original input
    readin = wav.read(sys.argv[1])
    samplerate = readin[0]
    input_data = pcm2float(readin[1], 'float32')

    # Read impulse response
    r2 = wav.read(sys.argv[2])
    samplerate2 = r2[0]
    impulse_response = pcm2float(r2[1], 'float32')

    print input_data.shape
    print impulse_response.shape

    print samplerate
    print samplerate2

    #sys.exit('stop')

    nFrames = input_data.shape[0]
    nChannels = input_data.shape[1]


    output_data = np.empty_like(input_data)
    #output_data.fill(-999)

    for ch in range(nChannels):
        output_data[:,ch] = ss.fftconvolve(input_data[:,ch], impulse_response[:,ch], mode='same')

    print output_data[0,0:5]

    wav.write('reverb.wav', samplerate, output_data)



if __name__ == '__main__':
    main()