
import sys
import numpy as np
import scipy.io.wavfile as wav
import scipy.signal as ss
from utility import pcm2float
import time




def main():
    readin = wav.read(sys.argv[1])
    samplerate = readin[0]
    input_data = pcm2float(readin[1], 'float32')

    nFrames = input_data.shape[0]
    nChannels = input_data.shape[1]

    gaussian_kernel = ss.gaussian(5,20)
    gaussian_kernel = gaussian_kernel / np.sum(gaussian_kernel)
    print gaussian_kernel.shape
    print gaussian_kernel

    output_data = np.empty_like(input_data)

    for ch in range(nChannels):
        start1 = time.time()
        output_data[:,ch] = ss.fftconvolve(input_data[:,ch], gaussian_kernel, mode='same')
        end1 = time.time()

        print "FFT convolve time: ",
        print end1 - start1

        start2 = time.time()
        output_data[:,ch] = ss.convolve(input_data[:,ch], gaussian_kernel, mode='same')
        end2 = time.time()

        print "Regular convolve time: ",
        print end2 - start2


    wav.write('blur.wav', samplerate, output_data)



if __name__ == '__main__':
    main()