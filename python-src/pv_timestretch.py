# python strict to try phase vocoder


import sys
import numpy as np
import scipy.io.wavfile as wav
from utility import pcm2float

def get_num_windows(num_frames, window_length, stride):
    return ((num_frames - window_length) / stride) + 1

def output_size(input_size, pitch_factor):
    return int(input_size * pitch_factor)



def stft(data, window_length, stride):
    num_windows = get_num_windows(data.shape[0], window_length, stride)

    stft_data = np.empty( (num_windows, window_length), dtype=np.complex64 )

    # Assume rectangular weight
    #weight = float(stride)/window_length

    # Use Hanning window
    hann_window = np.hanning(window_length)

    for i in range(num_windows):
        start_idx = i * stride

        stft_data[i] = np.fft.fft( hann_window * data[start_idx : start_idx + window_length] )

    return stft_data


def inverse_stft(stft_data, window_length, stride, pitch_factor, nOutputFrames, num_windows):

    # We'll have extra zeroes at the end where the windows didn't quite reach
    output_channel_data = np.zeros( nOutputFrames, dtype=np.float )

    for win_idx in range(num_windows):
        windowed_output_data = np.real( np.fft.ifft( stft_data[win_idx] ) )

        start_idx = int(pitch_factor * win_idx * stride)
        output_channel_data[start_idx : (start_idx + window_length)] \
            = output_channel_data[start_idx : (start_idx + window_length)] \
                + (windowed_output_data)

    print output_channel_data

    return output_channel_data


def main():
    readin = wav.read(sys.argv[1])
    samplerate = readin[0]
    input_data = pcm2float(readin[1], 'float32')

    nFrames = input_data.shape[0]
    nChannels = input_data.shape[1]

    window_length = int(sys.argv[3])
    stride = int(sys.argv[4])

    pitch_factor = 2#np.sqrt(np.sqrt(2.0))

    num_windows = get_num_windows(nFrames, window_length, stride)


    nOutputFrames = output_size(nFrames, pitch_factor)
    output_data = np.zeros( (nOutputFrames, nChannels), dtype=np.float32 )   # Zero just in case
    output_name = sys.argv[2]

    # Audio data is arranged in shape (#frames, #channels)
    print input_data.shape


    # Prepare for interpolation

    # select x-values that equally divide the range given
    # by output files
    interp_xvals = np.arange(nFrames) * (float(nOutputFrames) / nFrames)
    default_output_xvals = np.arange(nOutputFrames)

    pitchshift_output_data = np.empty_like( input_data )


    # 
    for ch in range(nChannels):
        channel_input_data = input_data[:,ch]
        stft_data = stft(channel_input_data, window_length, stride)

        # Set up phase buffers (local and cumulative)
        last_phase = np.zeros( window_length, dtype=np.float32 )
        cum_phase = np.zeros( window_length, dtype=np.float32 ) 

        # Don't zero out half of the window for now
        # (When you do, just do a quick numpy)



        for win_idx in range(num_windows):

            re_data = np.real(stft_data[win_idx])
            im_data = np.imag(stft_data[win_idx])

            magnitude_data = np.absolute(stft_data[win_idx])
            current_phase_data = np.arctan2(im_data, re_data)

            phase_difference_data = current_phase_data - last_phase

            indices = np.arange( window_length )

            nonextran_phase_difference_data \
                = phase_difference_data \
                    - ((2.0 * np.pi * stride * indices) / window_length)

            ranged_phase_difference_data \
                = np.fmod(nonextran_phase_difference_data + np.pi, 2 * np.pi) - np.pi

            true_frequency_data \
                = ((2 * np.pi * indices) / window_length)  \
                    + (ranged_phase_difference_data / stride)

            cum_phase \
                = cum_phase + (pitch_factor * stride * true_frequency_data)

            # Write back
            stft_data[win_idx] = magnitude_data * np.exp(1j * cum_phase)

            # TRY ZEROING
            stft_data[win_idx, (window_length / 2) : -1] = 0.0 + 0.0j;




        channel_output_data = inverse_stft(stft_data, window_length, \
            stride, pitch_factor, nOutputFrames, num_windows)

        output_data[:,ch] = channel_output_data

        pitchshift_channel_output_data = np.interp( \
            interp_xvals, default_output_xvals, channel_output_data)

        pitchshift_output_data[:,ch] = pitchshift_channel_output_data


    wav.write(output_name, samplerate, output_data)
    wav.write('pitchshift.wav', samplerate, pitchshift_output_data)






def array_dimensions():
    input_data = wav.read(sys.argv[1])

    print input_data[0]
    print input_data[1].shape

    np.set_printoptions(threshold=np.nan)
    output_data = pcm2float(input_data[1], 'float32')

    wav.write(sys.argv[2], input_data[0], output_data)
    #wav.write(sys.argv[2], input_data[0], input_data[1])


    # Use numpy interp at the end

if __name__ == '__main__':
    main()




