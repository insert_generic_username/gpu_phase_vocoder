
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <sndfile.h>
#include <fftw3.h>

#include <assert.h>


// Copied 17-10-2014 from wav_fn.cpp

// Does WAV I/O, and FFTW experimentation




/********** NOTES FROM PREVIOUS FILE ************/

// Tentative "make" command: g++ -lsndfile -lm wav_fn.cpp


// For now, just read in and write out a WAV (16-bit signed PCM) file
// using libsndfile

// based off generate.c in the examples

// args: input name, output name

// For now, don't do any real "buffering"




// For STFT, assume window_length is a multiple of stride

/***********************************************/


#define FREQ_FILTER_TEST 1
#define NAIVE_PITCH_TEST 0

#define DEBUG_OUTPUT 0

#define OVERLAP_ADD 1


using std::cerr;
using std::endl;


float ** separate_channels(float *input_data, int num_frames, int num_channels){
    float **channel_input_data = new float*[num_channels];
    for (int i = 0; i < num_channels; i++){
        channel_input_data[i] = new float[num_frames];
    }

    for (int i = 0; i < num_frames; i++){
        for (int j = 0; j < num_channels; j++){
            channel_input_data[j][i] = input_data[ i * num_frames + j];
        }
    }

    return channel_input_data;
}



// For now, ignore effects at the edge
fftw_complex ** stft(fftw_complex *complex_input_data, 
        fftw_complex **windowed_input_data,
        fftw_complex **stft_result,
        int num_frames, int window_length, int stride){

    int num_windows = (num_frames - window_length - 1) / stride;

    for (int i = 0; i < num_windows; i++){


        int start_idx = i * stride;

        for (int j = 0; j < window_length; j++){

#if OVERLAP_ADD
            windowed_input_data[i][j][0] = complex_input_data[start_idx + j][0]
                / (window_length / stride);
            windowed_input_data[i][j][1] = complex_input_data[start_idx + j][1]
                / (window_length / stride);
#else

            windowed_input_data[i][j][0] = complex_input_data[start_idx + j][0];
            windowed_input_data[i][j][1] = complex_input_data[start_idx + j][1];
#endif

        }

        fftw_plan plan_forward = fftw_plan_dft_1d(window_length, 
            windowed_input_data[i], stft_result[i],
            FFTW_FORWARD, FFTW_ESTIMATE);


        fftw_execute( plan_forward );
    }

    // NOTE: Remember to delete windowed input data

    return stft_result;

}


fftw_complex * inverse_stft(fftw_complex **stft_data_windowed, 
        fftw_complex ** windowed_output_data,
        fftw_complex * output_data,
        int num_frames, int window_length, int stride){


    int num_windows = (num_frames - window_length - 1) / stride;



    for (int i = 0; i < num_windows; i++){


        fftw_plan plan_backward = fftw_plan_dft_1d( window_length, stft_data_windowed[i],
            windowed_output_data[i], FFTW_BACKWARD, FFTW_ESTIMATE );

        fftw_execute( plan_backward );

        int start_idx = i * stride;

        // Do effectively "add-overlap"
        // (or "overlap-overwrite" if we so choose with our flag)
        for (int j = 0; j < window_length; j++){


#if OVERLAP_ADD

            output_data[start_idx + j][0] 
                    += windowed_output_data[i][j][0]
                        / window_length;
            output_data[start_idx + j][1] 
                    += windowed_output_data[i][j][1]
                        / window_length;

#else

            output_data[start_idx + j][0] 
                    = windowed_output_data[i][j][0]
                        / window_length;
            output_data[start_idx + j][1] 
                    = windowed_output_data[i][j][1]
                        / window_length;
#endif

#if DEBUG_OUTPUT
            cerr << windowed_output_data[i][j][0] << endl;
            cerr << windowed_output_data[i][j][1] << endl << endl;
#endif

        }

    }

    return output_data;
}



int main(int argc, char **argv){

    if (argc != 3){
        cerr << "Incorrect number of arguments, exiting\n";
        exit(EXIT_FAILURE);
    }

    SNDFILE *in_file, *out_file;
    SF_INFO in_file_info, out_file_info;

    int amt_read;



    // Open files

    in_file = sf_open(argv[1], SFM_READ, &in_file_info);
    if (!in_file){
        cerr << "Cannot open input file, exiting\n";
        exit(EXIT_FAILURE);
    }


    // Allocate memory to store audio
    // (it's statically allocated for now)
    float *input_data = new float[in_file_info.frames * in_file_info.channels];

    amt_read = sf_read_float(in_file, input_data, 
        in_file_info.frames * in_file_info.channels);

    assert(amt_read == in_file_info.frames * in_file_info.channels);




#if 0
    float **channel_input_data = separate_channels(input_data, 
        in_file_info.frames, in_file_info.channels);

    float **channel_output_data = new float*[in_file_info.channels];
    for (int i = 0; i < in_file_info.channels; i++){
        channel_output_data[i] = new float[in_file_info.frames];
    }
#endif


    // Divide back by constant factor, and take real values


    // Run STFT
    int window_length = 10000;
    int stride = 1000;

    int num_windows = (in_file_info.frames - window_length - 1) / stride;


    fftw_complex *input_data_complex = (fftw_complex *) 
                fftw_malloc(sizeof(fftw_complex) * in_file_info.frames);



    fftw_complex ** windowed_input_data 
            = (fftw_complex**) malloc( sizeof(fftw_complex*) * num_windows );

    fftw_complex ** stft_complex 
            = (fftw_complex**) malloc( sizeof(fftw_complex*) * num_windows );


    fftw_complex ** windowed_output_data 
            = (fftw_complex **) malloc( sizeof(fftw_complex*) * num_windows );

    fftw_complex *complex_output_data 
            = (fftw_complex *) fftw_malloc( sizeof(fftw_complex) * in_file_info.frames );

    for (int i = 0; i < num_windows; i++){
        windowed_input_data[i] 
                = (fftw_complex *) fftw_malloc( sizeof(fftw_complex) * window_length );

        stft_complex[i] 
                = (fftw_complex *) fftw_malloc( sizeof(fftw_complex) * window_length );

        windowed_output_data[i] 
                = (fftw_complex*) fftw_malloc( sizeof(fftw_complex) * window_length );
    }

    float *output_data = new float[in_file_info.frames * in_file_info.channels];


    for (int ch = 0; ch < in_file_info.channels; ch++){


        /* As a test. FFT, zero out some frequencies, and iFFT */

        // Store audio data as complex numbers

        for (int i = 0; i < in_file_info.frames; i++){
            input_data_complex[i][0] = input_data[i * in_file_info.channels + ch];
            input_data_complex[i][1] = 0.0;
        }



        stft(input_data_complex, 
            windowed_input_data, stft_complex,
            in_file_info.frames, window_length, stride);


#if FREQ_FILTER_TEST

        for (int win_index = 0; win_index < num_windows; win_index++){


            // Test filtering out into frequency ranges (in Hz)
            int freq_range_low = 1000;
            int freq_range_high = 5000;

            for (int i = 0; i < freq_range_low * 
                    window_length / in_file_info.samplerate; i++){

                stft_complex[win_index][i][0] = 0.0;
                stft_complex[win_index][i][1] = 0.0;
            }

            for (int i = freq_range_high * 
                    window_length / in_file_info.samplerate;
                    i < window_length / 2; i++){

                stft_complex[win_index][i][0] = 0.0;
                stft_complex[win_index][i][1] = 0.0;
            }

            //Zero out upper half to avoid backwards effect
            for(int i = 0; i < window_length / 2; i++){
                stft_complex[win_index][window_length - i][0] = 0.0;
                stft_complex[win_index][window_length - i][1] = 0.0;
            }
        }

#endif

#if NAIVE_PITCH_TEST
        //
        for (int win_index = 0; win_index < num_windows; win_index++){
            for (int i = 0; i < window_length / 4; i++){
                stft_complex[win_index][2*i][0] *= stft_complex[win_index][i][0];
                stft_complex[win_index][2*i][1] *= stft_complex[win_index][i][1];
                stft_complex[win_index][2*i+1][0] = 0.0;
                stft_complex[win_index][2*i+1][1] = 0.0;
            }

            //Zero out upper half to avoid backwards effect
            for(int i = 0; i < window_length / 2; i++){
                stft_complex[win_index][window_length - i][0] = 0.0;
                stft_complex[win_index][window_length - i][1] = 0.0;
            }
        }
#endif


        // Inverse FFT

        fftw_complex *istft_result = inverse_stft(stft_complex,
            windowed_output_data,
            complex_output_data,
            in_file_info.frames, window_length, stride);


        for (int i = 0; i < in_file_info.frames; i++){
            // Remember to normalize the inverse Fourier transform

            // However, for STFT, normalization window is much smaller!!
            // (Handle in inverse_stft)
            output_data[i * in_file_info.channels + ch] 
                    = istft_result[i][0];
        }

    }



    // Output file info will essentially be the same.
    // The one exception is probably that we want to
    // mandate this output format (reconsider this?)
    out_file_info = in_file_info;
    //out_file_info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;

    out_file = sf_open(argv[2], SFM_WRITE, &out_file_info);

    if (!out_file){
        cerr << "Cannot open output file, exiting\n";
        exit(EXIT_FAILURE);
    }



    sf_write_float(out_file, output_data, amt_read);

    sf_close(in_file);
    sf_close(out_file);

    return EXIT_SUCCESS;
}