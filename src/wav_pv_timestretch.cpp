
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <sndfile.h>
#include <fftw3.h>

#include <assert.h>



/********** NOTES FROM PREVIOUS FILE ************/

// Tentative "make" command: g++ -lsndfile -lm wav_fn.cpp


// For now, just read in and write out a WAV (16-bit signed PCM) file
// using libsndfile

// based off generate.c in the examples

// args: input name, output name

// For now, don't do any real "buffering"




// For STFT, assume window_length is a multiple of stride

/***********************************************/
 
SF_INFO debug_input_sfinfo;
int debug_cur_channel_num;





#define PI 3.14159265358979

#define PITCH_FACTOR (sqrt(2))

#define DEBUG_AUDIO_OUTPUT 1


//#define OPTIONAL_HALF_WINDOW_SIZE(x) ((x)/2)
#define OPTIONAL_HALF_WINDOW_SIZE(x) ((x))
#define HALF_WINDOW_ZERO 0

#define OVERLAP_ADD 1


enum WindowType {GAUSSIAN_WINDOW, TRIANGLE_WINDOW, RECTANGLE_WINDOW};
WindowType cur_win_type = GAUSSIAN_WINDOW;

using std::cerr;
using std::cout;
using std::endl;






int getNumWindows(int num_frames, int window_length, int stride){
    return (num_frames - window_length - 1) / stride;
}



/* Given the input data placed into complex form (with imaginary
part set to 0), windows the data and calculates their Fourier transform
(in effect, calculating the STFT)

    complex_input_data: (described above)
    windowed_input_data: Location to store time-domain audio data
        after windowing
    stft_result: Location to store frequency-domain audio data
        (windowed)
 */
void stft(fftw_complex *complex_input_data, 
        fftw_complex **windowed_input_data,
        fftw_complex **stft_result,
        int num_frames, int window_length, int stride){


    int num_windows = getNumWindows(num_frames, window_length, stride);



    for (int i = 0; i < num_windows; i++){

        // Determines where the i-th window will start
        // grabbing data from in our original array
        int start_idx = i * stride;

        // (For each location in the i-th window...)
        for (int j = 0; j < window_length; j++){

            /* Calculate the weights for this location within
            the window based on the windowing strategy we use */
            float center = j - (window_length / 2);
            float std = (window_length / 6);

            float weight;

            switch (cur_win_type){

                case GAUSSIAN_WINDOW:
                    weight = 2000 * 1 / (sqrt(2*PI) * std)
                        * exp(-1.0 * (center * center) / (2.0 * std * std));
                    break;

                case TRIANGLE_WINDOW:
                    // OPTIONAL_HALF_WINDOW_SIZE
                    weight = (1.0 / window_length) * (j - window_length / 2);
                    break;

                case RECTANGLE_WINDOW:
                    weight = ((float)stride)/window_length;
                    break;

                default:
                    cerr << "Default case reached in window sizing. Exiting\n";
                    exit(EXIT_FAILURE);

            }


            windowed_input_data[i][j][0] = weight * complex_input_data[start_idx + j][0];
            windowed_input_data[i][j][1] = weight * complex_input_data[start_idx + j][1];
        }


        /* Execute FFT */

        fftw_plan plan_forward = fftw_plan_dft_1d(window_length, 
            windowed_input_data[i], stft_result[i],
            FFTW_FORWARD, FFTW_ESTIMATE);

        fftw_execute( plan_forward );
    }

}



/* Given the input data (the modified windowed frequency data), 
take inverse FFT, and either overlap-add
or overlap-overwrite data back into non-windowed output array.

    stft_data_windowed: (frequency-domain input data described above)
    windowed_output_data: Location to store time-domain audio data
        after inverse STFT
    stf_result: Location to store time-domain audio data
        after combining all windows
 */
void inverse_stft(fftw_complex **stft_data_windowed, 
        fftw_complex ** windowed_output_data,
        fftw_complex * output_data,
        int num_frames, int window_length, int stride){


    int num_windows = getNumWindows(num_frames, window_length, stride);


    /* Loop through every window in the frequency-domain data array */
    for (int i = 0; i < num_windows; i++){

        /* Calculate inverse FFT for this window */

        fftw_plan plan_backward = fftw_plan_dft_1d( window_length, stft_data_windowed[i],
            windowed_output_data[i], FFTW_BACKWARD, FFTW_ESTIMATE );

        fftw_execute( plan_backward );


        ////////////////////////////////////////////////////////////////




        /* Starting index in the combined audio array where we will
        begin writing this array */
        // CHECK for rounding error
        int start_idx = i * stride * PITCH_FACTOR;

        // Do effectively "add-overlap"
        for (int j = 0; j < window_length; j++){

            #if OVERLAP_ADD

                output_data[start_idx + j][0] 
                        += windowed_output_data[i][j][0]
                            / window_length;
                output_data[start_idx + j][1] 
                        += windowed_output_data[i][j][1]
                            / window_length;

            #else

                output_data[start_idx + j][0] 
                        = windowed_output_data[i][j][0]
                            / window_length;
                output_data[start_idx + j][1] 
                        = windowed_output_data[i][j][1]
                            / window_length;

            #endif
        }

    }


}



int main(int argc, char **argv){

    if (argc != 3){
        cerr << "Incorrect number of arguments, exiting\n";
        exit(EXIT_FAILURE);
    }

    SNDFILE *in_file, *out_file;
    SF_INFO in_file_info, out_file_info;

    int amt_read;



    // Open files

    in_file = sf_open(argv[1], SFM_READ, &in_file_info);
    if (!in_file){
        cerr << "Cannot open input file, exiting\n";
        exit(EXIT_FAILURE);
    }


    // Allocate memory to store audio
    // (it's statically allocated for now)
    float *input_data = new float[in_file_info.frames * in_file_info.channels];

    amt_read = sf_read_float(in_file, input_data, 
        in_file_info.frames * in_file_info.channels);

    assert(amt_read == in_file_info.frames * in_file_info.channels);






    // Run STFT
    int window_length = 10000;
    int stride = 2500;






    int num_windows = getNumWindows(in_file_info.frames, window_length, stride);


    fftw_complex *input_data_complex = (fftw_complex *) 
                fftw_malloc(sizeof(fftw_complex) * in_file_info.frames);



    fftw_complex ** windowed_input_data 
            = (fftw_complex**) malloc( sizeof(fftw_complex*) * num_windows );

    fftw_complex ** stft_complex 
            = (fftw_complex**) malloc( sizeof(fftw_complex*) * num_windows );

    // Magnitudes of *current* frame
    float *magnitudes = new float[OPTIONAL_HALF_WINDOW_SIZE(window_length)];

    // Buffer for current and previous phase
    float **phase_swap_buffers = new float*[2];
    phase_swap_buffers[0] = new float[OPTIONAL_HALF_WINDOW_SIZE(window_length)]();
    phase_swap_buffers[1] = new float[OPTIONAL_HALF_WINDOW_SIZE(window_length)]();

    // Indices to indicate which of the above is which at any given time
    int current_phase_idx = 0;
    int prev_phase_idx = 1;

    // Buffer for cumulative phase 
    float *cum_phase = new float[OPTIONAL_HALF_WINDOW_SIZE(window_length)];



    // TEMPORARY BUFFERS (probably can optimize this somehow)
    float *phase_difference = new float[OPTIONAL_HALF_WINDOW_SIZE(window_length)];

    float *nonextran_phase_difference = new float[OPTIONAL_HALF_WINDOW_SIZE(window_length)];

    float *ranged_phase_difference = new float[OPTIONAL_HALF_WINDOW_SIZE(window_length)];

    float *true_freq = new float[OPTIONAL_HALF_WINDOW_SIZE(window_length)];






    fftw_complex ** windowed_output_data 
            = (fftw_complex **) malloc( sizeof(fftw_complex*) * num_windows );

    fftw_complex *complex_output_data 
            = (fftw_complex *) fftw_malloc( sizeof(fftw_complex) * (int)(in_file_info.frames * PITCH_FACTOR) );

    for (int i = 0; i < num_windows; i++){
        windowed_input_data[i] 
                = (fftw_complex *) fftw_malloc( sizeof(fftw_complex) * window_length );

        stft_complex[i] 
                = (fftw_complex *) fftw_malloc( sizeof(fftw_complex) * window_length );

        windowed_output_data[i] 
                = (fftw_complex*) fftw_malloc( sizeof(fftw_complex) * window_length );
    }

    float *output_data = new float[(int)
        (in_file_info.frames * PITCH_FACTOR) * in_file_info.channels];


    for (int ch = 0; ch < in_file_info.channels; ch++){


        // Need to zero out cumulative phase!
        for (int p = 0; p < OPTIONAL_HALF_WINDOW_SIZE(window_length); p++){
            cum_phase[p] = 0.0;
        }


        /* As a test. FFT, zero out some frequencies, and iFFT */

        // Store audio data as complex numbers

        for (int i = 0; i < in_file_info.frames; i++){
            input_data_complex[i][0] = input_data[i * in_file_info.channels + ch];
            input_data_complex[i][1] = 0.0;
        }



        stft(input_data_complex, 
            windowed_input_data, stft_complex,
            in_file_info.frames, window_length, stride);




        for (int win_index = 0; win_index < num_windows; win_index++){

            //Zero out upper half to avoid backwards effect
            // (THIS MAY BREAK THINGS)

#if HALF_WINDOW_ZERO
            for(int i = 0; i < window_length / 2; i++){
                stft_complex[win_index][window_length - i][0] = 0.0;
                stft_complex[win_index][window_length - i][1] = 0.0;
            }
#endif



            // Calculate magnitudes and phase angles for this FFT window
            for (int i = 0; i < OPTIONAL_HALF_WINDOW_SIZE(window_length); i++){

                float re = stft_complex[win_index][i][0];
                float im = stft_complex[win_index][i][1];

                magnitudes[i] = sqrt( re * re + im * im );

                phase_swap_buffers[current_phase_idx][i]
                    = atan2(im, re);

                phase_difference[i]
                    = phase_swap_buffers[current_phase_idx][i]
                        - phase_swap_buffers[prev_phase_idx][i];

                // !!! The dividing factor might have to
                        // be window_length / 2 b/c of our halved windows
                nonextran_phase_difference[i]
                    = phase_difference[i]
                        - (2 * PI * i * stride / window_length);

                ranged_phase_difference[i] 
                    = fmod(nonextran_phase_difference[i]+PI, 2*PI) - PI;

                // (SAME HERE)
                    // Also that divide by stride step seems a bit weird
                true_freq[i] = 2*PI*i/window_length + ranged_phase_difference[i]/stride;

                cum_phase[i] += PITCH_FACTOR * stride * true_freq[i];

                stft_complex[win_index][i][0] = magnitudes[i] 
                    * cos(cum_phase[i]);
                stft_complex[win_index][i][1] = magnitudes[i] 
                    * sin(cum_phase[i]);
            }


            current_phase_idx = (current_phase_idx + 1) % 2;
            prev_phase_idx = (prev_phase_idx + 1) % 2;

        }



        // Inverse FFT

        inverse_stft(stft_complex,
            windowed_output_data,
            complex_output_data,
            in_file_info.frames, window_length, stride);


        for (int i = 0; i < (int)(in_file_info.frames * PITCH_FACTOR); i++){
            // Remember to normalize the inverse Fourier transform

            // However, for STFT, normalization window is much smaller!!
            // (Handle in inverse_stft)
            output_data[i * in_file_info.channels + ch] 
                    = complex_output_data[i][0];
        }

    }



    // Output file info will essentially be the same.
    // The one exception is probably that we want to
    // mandate this output format (reconsider this?)
    out_file_info = in_file_info;
    //out_file_info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;


    out_file = sf_open(argv[2], SFM_WRITE, &out_file_info);

    // TEMPORARY - output time-shifted version
    // MUST GO AFTER the outfile
    out_file_info.frames = (int)(in_file_info.frames * PITCH_FACTOR);


    if (!out_file){
        cerr << "Cannot open output file, exiting\n";
        exit(EXIT_FAILURE);
    }


    int num_items_written = sf_write_float(out_file, output_data, out_file_info.channels * out_file_info.frames);


    sf_close(in_file);
    sf_close(out_file);

    return EXIT_SUCCESS;
}