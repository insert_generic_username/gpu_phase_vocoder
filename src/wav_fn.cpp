
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <sndfile.h>


// Tentative "make" command: g++ -lsndfile -lm wav_fn.cpp


// For now, just read in and write out a WAV (16-bit signed PCM) file
// using libsndfile

// based off generate.c in the examples

// args: input name, output name

// For now, don't do any real "buffering"


#define MAX_READ_SIZE (1 << 26)



using std::cerr;

int main(int argc, char **argv){

    if (argc != 3){
        cerr << "Incorrect number of arguments, exiting\n";
        exit(EXIT_FAILURE);
    }

    SNDFILE *in_file, *out_file;
    SF_INFO in_file_info, out_file_info;

    int amt_read;



    // Open files

    in_file = sf_open(argv[1], SFM_READ, &in_file_info);
    if (!in_file){
        cerr << "Cannot open input file, exiting\n";
        exit(EXIT_FAILURE);
    }

    // Output file info will essentially be the same.
    // The one exception is probably that we want to
    // mandate this output format (reconsider this?)
    out_file_info = in_file_info;
    //out_file_info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;

    out_file = sf_open(argv[2], SFM_WRITE, &out_file_info);
    if (!out_file){
        cerr << "Cannot open output file, exiting\n";
        exit(EXIT_FAILURE);
    }


    // Allocate memory to store audio
    // (it's statically allocated for now)
    static float input_data[MAX_READ_SIZE];

    amt_read = sf_read_float(in_file, input_data, MAX_READ_SIZE);
    sf_write_float(out_file, input_data, amt_read);

    sf_close(in_file);
    sf_close(out_file);

    return EXIT_SUCCESS;
}