
#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include <sndfile.h>
#include <fftw3.h>

#include <assert.h>


// Copied 17-10-2014 from wav_fn.cpp

// Does WAV I/O, and FFTW experimentation




/********** NOTES FROM PREVIOUS FILE ************/

// Tentative "make" command: g++ -lsndfile -lm wav_fn.cpp


// For now, just read in and write out a WAV (16-bit signed PCM) file
// using libsndfile

// based off generate.c in the examples

// args: input name, output name

// For now, don't do any real "buffering"

/***********************************************/


#define FREQ_FILTER 1


using std::cerr;


float ** separate_channels(float *input_data, int num_frames, int num_channels){
    float **channel_input_data = new float*[num_channels];
    for (int i = 0; i < num_channels; i++){
        channel_input_data[i] = new float[num_frames];
    }

    for (int i = 0; i < num_frames; i++){
        for (int j = 0; j < num_channels; j++){
            channel_input_data[j][i] = input_data[ i * num_frames + j];
        }
    }

    return channel_input_data;
}


int main(int argc, char **argv){

    if (argc != 3){
        cerr << "Incorrect number of arguments, exiting\n";
        exit(EXIT_FAILURE);
    }

    SNDFILE *in_file, *out_file;
    SF_INFO in_file_info, out_file_info;

    int amt_read;



    // Open files

    in_file = sf_open(argv[1], SFM_READ, &in_file_info);
    if (!in_file){
        cerr << "Cannot open input file, exiting\n";
        exit(EXIT_FAILURE);
    }


    // Allocate memory to store audio
    // (it's statically allocated for now)
    float *input_data = new float[in_file_info.frames * in_file_info.channels];

    amt_read = sf_read_float(in_file, input_data, 
        in_file_info.frames * in_file_info.channels);

    assert(amt_read == in_file_info.frames * in_file_info.channels);


#if 0
    float **channel_input_data = separate_channels(input_data, 
        in_file_info.frames, in_file_info.channels);

    float **channel_output_data = new float*[in_file_info.channels];
    for (int i = 0; i < in_file_info.channels; i++){
        channel_output_data[i] = new float[in_file_info.frames];
    }
#endif


    // Divide back by constant factor, and take real values

    float *output_data = new float[in_file_info.frames * in_file_info.channels];



    for (int ch = 0; ch < in_file_info.channels; ch++){
        /* As a test. FFT, zero out some frequencies, and iFFT */

        // Store audio data as complex numbers
        fftw_complex *input_data_complex = (fftw_complex *) 
                fftw_malloc(sizeof(fftw_complex) * in_file_info.frames);

        for (int i = 0; i < in_file_info.frames; i++){
            input_data_complex[i][0] = input_data[i * in_file_info.channels + ch];
            input_data_complex[i][1] = 0.0;
        }


        // Place to store FFT
        fftw_complex *fft_data_complex = (fftw_complex *) 
                fftw_malloc(sizeof(fftw_complex) * in_file_info.frames);

        fftw_plan plan_forward = fftw_plan_dft_1d(in_file_info.frames, 
            input_data_complex, fft_data_complex,
            FFTW_FORWARD, FFTW_ESTIMATE);


        fftw_execute( plan_forward );


#if FREQ_FILTER

        // Test filtering out into frequency ranges (in Hz)
        int freq_range_low = 1000;
        int freq_range_high = 5000;

        for (int i = 0; i < freq_range_low * 
                (in_file_info.frames / in_file_info.samplerate); i++){

            fft_data_complex[i][0] = 0.0;
            fft_data_complex[i][1] = 0.0;
        }

        for (int i = freq_range_high * 
                (in_file_info.frames / in_file_info.samplerate);
                i < in_file_info.frames / 2; i++){

            fft_data_complex[i][0] = 0.0;
            fft_data_complex[i][1] = 0.0;
        }

        //Zero out upper half to avoid backwards effect
        for(int i = 0; i < in_file_info.frames / 2; i++){
            fft_data_complex[in_file_info.frames - i][0] = 0.0;
            fft_data_complex[in_file_info.frames - i][1] = 0.0;
        }

#endif



        // Inverse FFT

        fftw_complex *ifft_result = ( fftw_complex* ) fftw_malloc(
            sizeof( fftw_complex ) * in_file_info.frames );

        fftw_plan plan_backward = fftw_plan_dft_1d( in_file_info.frames, fft_data_complex,
            ifft_result, FFTW_BACKWARD, FFTW_ESTIMATE );

        fftw_execute( plan_backward );


        for (int i = 0; i < in_file_info.frames; i++){
            // Remember to normalize the inverse Fourier transform
            output_data[i * in_file_info.channels + ch] 
                    = ifft_result[i][0] / in_file_info.frames;
        }

    }



    // Output file info will essentially be the same.
    // The one exception is probably that we want to
    // mandate this output format (reconsider this?)
    out_file_info = in_file_info;
    //out_file_info.format = SF_FORMAT_WAV | SF_FORMAT_PCM_16;

    out_file = sf_open(argv[2], SFM_WRITE, &out_file_info);

    if (!out_file){
        cerr << "Cannot open output file, exiting\n";
        exit(EXIT_FAILURE);
    }



    sf_write_float(out_file, output_data, amt_read);

    sf_close(in_file);
    sf_close(out_file);

    return EXIT_SUCCESS;
}